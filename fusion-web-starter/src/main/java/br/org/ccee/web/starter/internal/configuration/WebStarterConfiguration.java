package br.org.ccee.web.starter.internal.configuration;

import br.org.ccee.fusion.web.rest.internal.configuration.WebRestConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.boot.autoconfigure.web.reactive.WebFluxAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({WebRestConfiguration.class})
@AutoConfigureAfter({TransactionAutoConfiguration.class, WebFluxAutoConfiguration.class})
public class WebStarterConfiguration {

}
