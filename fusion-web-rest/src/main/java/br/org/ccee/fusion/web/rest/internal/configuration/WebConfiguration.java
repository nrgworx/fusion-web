package br.org.ccee.fusion.web.rest.internal.configuration;

import br.org.ccee.fusion.web.rest.api.component.PageableParser;
import br.org.ccee.fusion.web.rest.api.component.PredicateParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

  @Autowired
  private PageableParser pageableParser;

  @Autowired
  private PredicateParser predicateParser;

  /*@Autowired
  private ObjectGraphParser objectGraphParser;*/

  @Bean
  @ConditionalOnMissingBean(RequestContextListener.class)
  public RequestContextListener requestContextListener() {
    return new RequestContextListener();
  }

  @Bean
  public CommonsRequestLoggingFilter requestLoggingFilter() {
    CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
    loggingFilter.setIncludeQueryString(true);
    loggingFilter.setIncludePayload(true);
    loggingFilter.setIncludeHeaders(false);
    loggingFilter.setIncludeClientInfo(true);
    return loggingFilter;
  }

  /*@Bean
  public HttpMessageConverters customConverters(
      JsonMessageConverter jsonMessageConverter,
      YamlMessageConverter yamlMessageConverter,
      XmlMessageConverter xmlMessageConverter,
      CsvMessageConverter csvMessageConverter,
      XlsxMessageConverter xlsxMessageConverter,
      PdfMessageConverter pdfMessageConverter) {
    return new HttpMessageConverters(
        jsonMessageConverter,
        yamlMessageConverter,
        xmlMessageConverter,
        csvMessageConverter,
        xlsxMessageConverter,
        pdfMessageConverter);
  }*/

  @Bean
  public MultipartResolver multipartResolver() {
      return new CommonsMultipartResolver();
  }

  /*@Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new RequestMetadataInterceptor());
  }*/

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer.ignoreUnknownPathExtensions(true)
      .favorPathExtension(true)
      .favorParameter(true)
      .parameterName("$format")
      .ignoreAcceptHeader(false)
      .useRegisteredExtensionsOnly(true)
      .defaultContentType(MediaType.APPLICATION_JSON)
      .mediaType("json", MediaType.APPLICATION_JSON)
      .mediaType("yaml", MediaType.valueOf("application/yaml"))
      .mediaType("xml", MediaType.APPLICATION_XML)
      .mediaType("csv", MediaType.valueOf("text/csv"))
      .mediaType("xlsx", MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
      .mediaType("pdf", MediaType.APPLICATION_PDF);
  }

  /*@Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> handlerMethodArgumentResolvers) {
    handlerMethodArgumentResolvers.add(new PageableMethodArgumentResolver(pageableParser));
    handlerMethodArgumentResolvers.add(new PredicateMethodArgumentResolver(predicateParser));
    handlerMethodArgumentResolvers.add(new ObjectGraphMethodArgumentResolver(objectGraphParser));
  }*/
}
