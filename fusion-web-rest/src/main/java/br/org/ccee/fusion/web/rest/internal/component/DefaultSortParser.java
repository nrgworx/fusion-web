package br.org.ccee.fusion.web.rest.internal.component;

import br.org.ccee.fusion.web.rest.api.component.SortParser;
import br.org.ccee.fusion.web.rest.api.exception.InvalidSortExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * {@inheritDoc}
 *
 * */
@Component
@ConditionalOnSingleCandidate(SortParser.class)
public class DefaultSortParser implements SortParser {

    private static final String INVALID_ORDER_SYNTAX = "Invalid order syntax for part: %s of: %s";
    private static final String SORT_EXPRESSION_SEPARATOR_CHAR = ",";
    private static final Pattern SORT_EXPRESSION_PART_PATTERN = Pattern.compile("(.+?)(asc|desc)?$");
    private static final Integer ONE = 1;
    private static final Integer TWO = 2;

    public static final Logger LOG = LoggerFactory.getLogger(DefaultSortParser.class);

    /**
     *
     * {@inheritDoc}
     *
     * */
    @Override
    public Sort parse(Optional<String> sortExpression) {
        try {
            return sortExpression.map(s -> Sort.by(getOrders(s)))
                .orElseGet(Sort::unsorted);
        } catch (InvalidSortExpressionException e) {
            LOG.error(e.getMessage());
            return Sort.unsorted();
        }
    }

    /**
     * @param sortExpression string with a sort expression
     * @return an List Object sorter
     * @throws InvalidSortExpressionException when experssion not match with a
     *                                        sorter
     */
    private List<Order> getOrders(String sortExpression) {
        List<Order> orders = new ArrayList<>();
        for (String sortExpressionPart : sortExpression.split(SORT_EXPRESSION_SEPARATOR_CHAR)) {
            Matcher matcher = SORT_EXPRESSION_PART_PATTERN.matcher(sortExpressionPart.toLowerCase());
            if (!matcher.find()) {
                throw new InvalidSortExpressionException(
                        String.format(INVALID_ORDER_SYNTAX, sortExpressionPart, sortExpression));
            }
            String propertyString = matcher.group(ONE);
            String directionString = matcher.group(TWO);
            if (sortExpressionPart.trim().split(" ").length > 1 && directionString == null) {
                throw new InvalidSortExpressionException(
                        String.format(INVALID_ORDER_SYNTAX, sortExpressionPart, sortExpression));
            }
            if (directionString == null || directionString.trim().isEmpty() || directionString.trim().equals("asc")) {
                orders.add(Order.asc(propertyString.trim()));
            } else if (directionString.trim().equals("desc")) {
                orders.add(Order.desc(propertyString.trim()));
            } else {
                throw new InvalidSortExpressionException(
                        String.format(INVALID_ORDER_SYNTAX, sortExpressionPart, sortExpression));
            }
        }
        return orders;
    }


}
