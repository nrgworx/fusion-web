package br.org.ccee.fusion.web.rest.api.exception;

/**
 * This class represents exceptions that are thrown during the entity sort
 * expression parsing process.
 * 
 * @since 1.0.0
 * @author thiago.assis
 */
public class InvalidSortExpressionException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public InvalidSortExpressionException(String message) {
    super(message);
  }

  public InvalidSortExpressionException(String sortExpression, Throwable cause) {
    super(sortExpression, cause);
  }

  public InvalidSortExpressionException() {
  }
}
