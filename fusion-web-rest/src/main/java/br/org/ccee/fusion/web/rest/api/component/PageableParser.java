package br.org.ccee.fusion.web.rest.api.component;

import java.util.Optional;
import java.util.OptionalInt;

import br.org.ccee.fusion.web.rest.api.exception.InvalidSortExpressionException;
import org.springframework.data.domain.Pageable;

public interface PageableParser {

	/**
	 * Default page namber when nothing receive
	 */
	int DEFAULT_PAGE_NUMBER = 0;

	/**
	 * Default page size when nothing receive
	 */
	int DEFAULT_PAGE_SIZE = 100;

	Pageable parse(final OptionalInt optionalPageNumber, final OptionalInt optionalPageSize,
				   final Optional<String> optionalSortExpression) throws InvalidSortExpressionException;

}
