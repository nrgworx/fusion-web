package br.org.ccee.fusion.web.rest.internal.component;

import br.org.ccee.fusion.web.rest.api.component.OperationStateMachineParser;
import br.org.ccee.fusion.web.rest.api.model.Operation;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnSingleCandidate(OperationStateMachineParser.class)
public class DefaultOperationStateMachineParser implements OperationStateMachineParser {

  private static final Logger LOG = LoggerFactory.getLogger(DefaultOperationStateMachineParser.class);
  private static final String UNDEFINED_STATE_OPERATION = "Operation state machine undefined";

  @Override
  public Operation parser(Optional<String> optionalStateOperation) {
    if (optionalStateOperation.isPresent()) {
      return Operation.valueOf(optionalStateOperation.get());
    } else {
      LOG.error(UNDEFINED_STATE_OPERATION);
      return Operation.UNDEFINED;
    }
  }

}
