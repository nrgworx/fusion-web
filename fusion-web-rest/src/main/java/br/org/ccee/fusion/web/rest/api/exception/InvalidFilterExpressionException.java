package br.org.ccee.fusion.web.rest.api.exception;

/**
 * This class represents exceptions that are thrown during the entity filter
 * expression parsing process.
 * 
 * @since 1.0.0
 * @author thiago.assis
 */
public class InvalidFilterExpressionException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidFilterExpressionException(String filterExpression) {
		super(filterExpression);
	}

	public InvalidFilterExpressionException(String filterExpression, Throwable cause) {
		super(filterExpression, cause);
	}

}
