package br.org.ccee.fusion.web.rest.api.component;

import br.org.ccee.fusion.web.rest.api.exception.InvalidSortExpressionException;
import org.springframework.data.domain.Sort;

import java.util.Optional;

/**
 * This class is responsible to define the methods used to handle the entity
 * sort feature.
 *
 *
 * @since 1.0.0
 * @author thiago.assis
 */
public interface SortParser {

	/**
	 * @param optionalSortExpression string with a sort expression
	 * @return an Object sorter
	 * @throws InvalidSortExpressionException when expression not match with a
	 *                                        sorter
	 */
	Sort parse(final Optional<String> optionalSortExpression);

}
