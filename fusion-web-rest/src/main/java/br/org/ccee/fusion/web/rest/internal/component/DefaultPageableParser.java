package br.org.ccee.fusion.web.rest.internal.component;

import java.util.Optional;
import java.util.OptionalInt;

import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import br.org.ccee.fusion.web.rest.api.component.PageableParser;
import br.org.ccee.fusion.web.rest.api.component.SortParser;
import br.org.ccee.fusion.web.rest.api.exception.InvalidSortExpressionException;

@Component
@ConditionalOnSingleCandidate(PageableParser.class)
public class DefaultPageableParser implements PageableParser {

	private final SortParser sortParser;

	public DefaultPageableParser(SortParser sortParser) {
		this.sortParser = sortParser;
	}

	@Override
	public Pageable parse(OptionalInt optionalPageNumber, OptionalInt optionalPageSize,
			Optional<String> optionalSortExpression) throws InvalidSortExpressionException {
		if (optionalSortExpression.isPresent()) {
			return PageRequest.of(optionalPageNumber.orElse(PageableParser.DEFAULT_PAGE_NUMBER),
					optionalPageSize.orElse(PageableParser.DEFAULT_PAGE_SIZE),
					this.sortParser.parse(optionalSortExpression));
		} else {
			return PageRequest.of(optionalPageNumber.orElse(PageableParser.DEFAULT_PAGE_NUMBER),
					optionalPageSize.orElse(PageableParser.DEFAULT_PAGE_SIZE));
		}
	}
}
