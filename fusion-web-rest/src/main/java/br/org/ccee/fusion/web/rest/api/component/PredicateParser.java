package br.org.ccee.fusion.web.rest.api.component;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.web.rest.api.exception.InvalidFilterExpressionException;

/**
 * This class is responsible to define the methods used to handle the entity
 * filter feature.
 *
 * @author thiago.assis
 * @since 1.0.0
 */
public interface PredicateParser {

	/**
	 * @param rootPath                 RootPath to create a predicate
	 * @param optionalFilterExpression expression to filter predicate
	 * @return an optinal predicate
	 * @throws InvalidFilterExpressionException when no match a filter expression
	 */
	Predicate parse(Class<?> type, final String filterExpression) throws InvalidFilterExpressionException;

}
