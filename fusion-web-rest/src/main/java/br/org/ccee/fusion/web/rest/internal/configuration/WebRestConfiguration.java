package br.org.ccee.fusion.web.rest.internal.configuration;

import br.org.ccee.fusion.web.core.internal.configuration.WebCoreConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ WebCoreConfiguration.class })
@EnableConfigurationProperties
@ComponentScan("br.org.ccee.fusion.web.rest")
public class WebRestConfiguration {

    private WebRestProperties webRestProperties;

    public WebRestConfiguration(WebRestProperties webRestProperties) {
        this.webRestProperties = webRestProperties;
    }

}
