package br.org.ccee.fusion.web.rest.api.exception;

/**
 * This class represents exceptions that are thrown during the entity expansion
 * expression parsing process.
 * 
 * @since 1.0.0
 * @author thiago.assis
 */
public class InvalidExpandExpressionException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidExpandExpressionException(String expandExpression, Throwable cause) {
		super(expandExpression, cause);
	}

}
