package br.org.ccee.fusion.web.rest.api.component;

import br.org.ccee.fusion.web.rest.api.exception.InvalidSortExpressionException;
import br.org.ccee.fusion.web.rest.api.model.Operation;
import java.util.Optional;

/**
 * This class is responsible to define the methods used to handle the entity operation state machine
 * feature.
 *
 * @author augusto.abreu
 * @since 1.0.0
 */
public interface OperationStateMachineParser {

  /**
   * Parser string into a operation state machine.
   *
   * @param optionalStateOperation string with a operation expression
   * @return an Object sorter
   * @throws InvalidSortExpressionException when expression not match with a operation
   */
  Operation parser(final Optional<String> optionalStateOperation);

}
