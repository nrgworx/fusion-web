package br.org.ccee.fusion.web.rest.internal;


import org.springframework.boot.builder.SpringApplicationBuilder;

/*@SpringBootApplication
@EnableWebFlux
@Import({WebRestConfiguration.class})
@ComponentScan({"br.org.ccee", "br.org.energia"})*/
public class Application {
    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).run(args);
    }
}
