package br.org.ccee.fusion.web.rest.internal.component;

import static org.junit.Assert.*;

import java.util.Optional;
import java.util.OptionalInt;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;

public class DefaultPageableParserTest {

  private DefaultPageableParser defaultPageableParser;

  @Before
  public void init() {
    defaultPageableParser = new DefaultPageableParser(new DefaultSortParser());
  }

  @Test
  public void parseComplete() {
    Pageable pageable = defaultPageableParser.parse(OptionalInt.of(0), OptionalInt.of(10), Optional.of("sort=name asc"));
    assertEquals(pageable.getPageNumber(), 0);
    assertEquals(pageable.getPageSize(), 10);
    assertTrue(pageable.getSort().isSorted());
  }

  @Test
  public void parseNoSort() {
    Pageable pageable = defaultPageableParser.parse(OptionalInt.of(0), OptionalInt.of(10), Optional.empty());
    assertEquals(pageable.getPageNumber(), 0);
    assertEquals(pageable.getPageSize(), 10);
    assertTrue(pageable.getSort().isUnsorted());
  }

  @Test
  public void parseNoPageSize() {
    Pageable pageable = defaultPageableParser.parse(OptionalInt.of(0), OptionalInt.empty(), Optional.empty());
    assertEquals(pageable.getPageNumber(), 0);
    assertEquals(pageable.getPageSize(), 100);
    assertTrue(pageable.getSort().isUnsorted());
  }

  @Test
  public void parseNoPageNumber() {
    Pageable pageable = defaultPageableParser.parse(OptionalInt.empty(), OptionalInt.empty(), Optional.empty());
    assertEquals(pageable.getPageNumber(), 0);
    assertEquals(pageable.getPageSize(), 100);
    assertTrue(pageable.getSort().isUnsorted());
  }

  @Test
  public void parseNoPageNumberNoPageSizeSorted() {
    Pageable pageable = defaultPageableParser.parse(OptionalInt.empty(), OptionalInt.empty(), Optional.of("sort=name asc"));
    assertEquals(pageable.getPageNumber(), 0);
    assertEquals(pageable.getPageSize(), 100);
    assertTrue(pageable.getSort().isSorted());
  }
}