package br.org.ccee.fusion.web.rest.internal.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//@Entity
//@Table(name="Person")
public class Person implements Serializable {

    public static final long serialVersionUID = 1L;

    //@Column(length = 128, nullable = false, updatable = false)
    private String id;

    //@Column(length = 128, nullable = false, updatable = false)
    private String firstName;

    //@Column(length = 128, nullable = false, updatable = false)
    private String lastName;

    public static Person getPersonMock() {
        Person person = new Person();
        person.setId("AbcdEF1234567");
        person.setFirstName("Jonh");
        person.setLastName("Happy");
        return person;
    }

    public static List<Person> getPersonListMock() {
        Person person = new Person();
        person.setId("AbcdEF1234567");
        person.setFirstName("Jonh");
        person.setLastName("Happy");

        Person person1 = new Person();
        person1.setId("1234567AbcdEF");
        person1.setFirstName("Mary");
        person1.setLastName("Jane");

        List<Person> persons = new ArrayList<>();
        persons.add(person);
        persons.add(person1);
        return persons;
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
