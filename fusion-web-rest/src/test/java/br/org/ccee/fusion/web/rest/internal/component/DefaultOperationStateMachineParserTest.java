package br.org.ccee.fusion.web.rest.internal.component;

import static org.junit.Assert.assertEquals;

import br.org.ccee.fusion.web.rest.api.model.Operation;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;

public class DefaultOperationStateMachineParserTest {

  private DefaultOperationStateMachineParser defaultOperationStateMachineParser;

  @Before
  public void init() {
    defaultOperationStateMachineParser = new DefaultOperationStateMachineParser();
  }

  @Test
  public void parseOperationSave() {
    Optional<String> optionalAprove = Optional.of("SAVE");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.SAVE.getName(), operation.getName());
  }

  @Test
  public void parseOperationCreate() {
    Optional<String> optionalAprove = Optional.of("CREATE");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.SAVE, operation.getParent());
  }

  @Test
  public void parseOperationRetrieve() {
    Optional<String> optionalAprove = Optional.of("RETRIEVE");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.RETRIEVE.getName(), operation.getName());
  }

  @Test
  public void parseOperationList() {
    Optional<String> optionalAprove = Optional.of("LIST");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.RETRIEVE, operation.getParent());
  }

  @Test
  public void parseOperationGet() {
    Optional<String> optionalAprove = Optional.of("GET");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.RETRIEVE, operation.getParent());
  }

  @Test
  public void parseOperationUpdate() {
    Optional<String> optionalAprove = Optional.of("UPDATE");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.SAVE, operation.getParent());
  }

  @Test
  public void parseOperationDelete() {
    Optional<String> optionalAprove = Optional.of("DELETE");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.DELETE.getName(), operation.getName());
  }

  @Test
  public void parseOperationAddOperator() {
    Optional<String> optionalAprove = Optional.of("ANYTHING");
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals("ANYTHING", operation.getName());
  }

  @Test
  public void parseOperationUndefined() {
    Optional<String> optionalAprove = Optional.empty();
    Operation operation = defaultOperationStateMachineParser.parser(optionalAprove);
    assertEquals(Operation.UNDEFINED.getName(), operation.getName());
  }

}
