package br.org.ccee.fusion.web.rest.internal.component;


import static org.junit.Assert.assertTrue;

import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Sort;

public class DefaultSortParserTest {

    private DefaultSortParser defaultSortParser;

    @Before
    public void init() {
        defaultSortParser = new DefaultSortParser();
    }

    @Test
    public void parseAsc() {
        Optional<String> orderOptional = Optional.of("sort=name asc");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.getOrderFor("sort=name").getDirection().isAscending());
    }

    @Test
    public void parseDesc() {
        Optional<String> orderOptional = Optional.of("sort=name desc");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.getOrderFor("sort=name").getDirection().isDescending());
    }

    @Test
    public void parseNoMatcherFind(){
        Optional<String> orderOptional = Optional.of("sort=id eeeea");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.isUnsorted());
    }

    @Test
    public void parseInvalidMoreOneDirection() {
        Optional<String> orderOptional = Optional.of("sort=old top down");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.isUnsorted());
    }

    @Test
    public void parseWithoutDirection() {
        Optional<String> orderOptional = Optional.of("sort=date");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.getOrderFor("sort=date").getDirection().isAscending());
    }

    @Test
    public void parseInvalidSorterWithoutEquals() {
        Optional<String> orderOptional = Optional.of("sort date");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.isUnsorted());
    }

    @Test
    public void parseComplexSorter() {
        Optional<String> orderOptional = Optional.of("sort=date asc, sort=name desc");
        Sort sort = defaultSortParser.parse(orderOptional);
        assertTrue(sort.getOrderFor("sort=date").getDirection().isAscending());
        assertTrue(sort.getOrderFor("sort=name").getDirection().isDescending());
    }
}