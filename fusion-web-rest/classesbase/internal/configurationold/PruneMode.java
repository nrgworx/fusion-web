package br.org.ccee.fusion.engine.controller.configuration;

public enum PruneMode {

  ALL, ENTITY, NONE;

}
