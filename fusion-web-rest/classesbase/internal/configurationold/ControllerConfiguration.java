package br.org.ccee.fusion.engine.controller.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.org.ccee.fusion.engine.core.configuration.CoreConfiguration;

@Configuration
@EnableConfigurationProperties(ControllerProperties.class)
@Import({ CoreConfiguration.class })
@ComponentScan("br.org.ccee.fusion.engine.controller")
public class ControllerConfiguration implements WebMvcConfigurer {

}
