package br.org.ccee.fusion.engine.controller.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.transaction.PlatformTransactionManager;
import br.org.ccee.fusion.engine.controller.internal.KryoRedisSerializer;

@Configuration
@EnableRedisRepositories(basePackages = "br.org.ccee.fusion.engine.controller")
public class RedisConfiguration {

  @Bean
  public RedisSerializer<Object> redisSerializer() {
    return new KryoRedisSerializer();
  }

  @Bean
  @Primary
  public RedisTemplate<String, Object> redisTemplate(PlatformTransactionManager transactionManager,
      RedisConnectionFactory redisConnectionFactory, RedisSerializer<Object> redisSerializer) {
    StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
    final RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
    redisTemplate.setConnectionFactory(redisConnectionFactory);
    // redisTemplate.setDefaultSerializer(redisSerializer);
    // redisTemplate.setHashKeySerializer(stringRedisSerializer);
    // redisTemplate.setHashValueSerializer(redisSerializer);
    redisTemplate.setKeySerializer(stringRedisSerializer);
    redisTemplate.setValueSerializer(redisSerializer);
    // redisTemplate.setStringSerializer(stringRedisSerializer);
    // TODO NullPointer! merda!
    //redisTemplate.setEnableTransactionSupport(true);
    redisTemplate.afterPropertiesSet();
    return redisTemplate;
  }
}
