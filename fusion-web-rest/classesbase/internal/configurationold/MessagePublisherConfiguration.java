package br.org.ccee.fusion.engine.controller.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import br.org.ccee.fusion.engine.controller.api.MessagePublisher;
import br.org.ccee.fusion.engine.controller.internal.RedisMessagePublisher;

@Configuration
public class MessagePublisherConfiguration {

  @Bean
  public MessagePublisher messagePublisher(RedisTemplate<String, Object> redisTemplate) {
    return new RedisMessagePublisher(redisTemplate);
  }
}
