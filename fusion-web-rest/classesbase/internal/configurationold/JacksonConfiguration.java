package br.org.ccee.fusion.engine.controller.configuration;

import org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.sett4.dataformat.xlsx.XlsxMapper;
import br.org.ccee.fusion.engine.controller.internal.ByteBuddyInterceptorJacksonSerializer;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class JacksonConfiguration {

  @Bean
  public Jdk8Module jdk8Module() {
    return new Jdk8Module();
  }

  @Bean
  public JavaTimeModule javaTimeModule() {
    return new JavaTimeModule();
  }

  @Bean
  public Hibernate5Module hibernate5Module() {
    Hibernate5Module hibernate5Module = new Hibernate5Module();
    hibernate5Module.disable(Hibernate5Module.Feature.FORCE_LAZY_LOADING);
    hibernate5Module.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
    return hibernate5Module;
  }

  @Bean
  public SimpleModule customModule() {
    SimpleModule customModule = new SimpleModule();
    customModule.addSerializer(ByteBuddyInterceptor.class, new ByteBuddyInterceptorJacksonSerializer());
    return customModule;
  }

  @Bean
  @Primary
  public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder(Jdk8Module jdk8Module,
      JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module,
      SimpleModule customModule) {
    return new Jackson2ObjectMapperBuilder()
        .createXmlMapper(false)
        .modules(jdk8Module, javaTimeModule, hibernate5Module, customModule)
        .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        //.serializationInclusion(JsonInclude.Include.NON_EMPTY);
  }

  @Bean
  @Primary
  public ObjectMapper objectMapper(Jdk8Module jdk8Module, JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module, SimpleModule customModule) {
    ObjectMapper objectMapper = new ObjectMapper();
    configureMapper(objectMapper, jdk8Module, javaTimeModule, hibernate5Module, customModule);
    return objectMapper;
  }

  @Bean("jsonMapper")
  public ObjectMapper jsonMapper(ObjectMapper objectMapper) {
    return objectMapper;
  }

  @Bean("yamlMapper")
  public ObjectMapper yamlMapper(Jdk8Module jdk8Module, JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module, SimpleModule customModule) {
    ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
    configureMapper(objectMapper, jdk8Module, javaTimeModule, hibernate5Module, customModule);
    return objectMapper;
  }

  @Bean
  public XmlMapper xmlMapper(Jdk8Module jdk8Module, JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module, SimpleModule customModule) {
    XmlMapper xmlMapper = new XmlMapper();
    configureMapper(xmlMapper, jdk8Module, javaTimeModule, hibernate5Module, customModule);
    return xmlMapper;
  }

  @Bean
  public CsvMapper csvMapper(Jdk8Module jdk8Module, JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module, SimpleModule customModule) {
    CsvMapper csvMapper = new CsvMapper();
    configureMapper(csvMapper, jdk8Module, javaTimeModule, hibernate5Module, customModule);
    return csvMapper;
  }

  @Bean
  public XlsxMapper xlsxMapper(Jdk8Module jdk8Module, JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module, SimpleModule customModule) {
    XlsxMapper xlsxMapper = new XlsxMapper();
    configureMapper(xlsxMapper, jdk8Module, javaTimeModule, hibernate5Module, customModule);
    return xlsxMapper;
  }

  private void configureMapper(ObjectMapper objectMapper, Jdk8Module jdk8Module, JavaTimeModule javaTimeModule, Hibernate5Module hibernate5Module, SimpleModule customModule) {
    objectMapper.registerModule(jdk8Module);
    objectMapper.registerModule(javaTimeModule);
    objectMapper.registerModule(hibernate5Module);
    objectMapper.registerModule(customModule);
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    //objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
  }
}
