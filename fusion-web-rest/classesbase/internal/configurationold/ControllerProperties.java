package br.org.ccee.fusion.engine.controller.configuration;

import java.io.File;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "fusion.engine.controller")
public class ControllerProperties implements InitializingBean {

  private String defaultMediaType;

  private PruneMode pruneMode;

  private File dataDirectory;

  public String getDefaultMediaType() {
    return defaultMediaType;
  }

  public void setDefaultMediaType(String defaultMediaType) {
    this.defaultMediaType = defaultMediaType;
  }

  public PruneMode getPruneMode() {
    return pruneMode;
  }

  public void setPruneMode(PruneMode pruneMode) {
    this.pruneMode = pruneMode;
  }

  public File getDataDirectory() {
    return dataDirectory;
  }

  public void setDataDirectory(File dataDirectory) {
    this.dataDirectory = dataDirectory;
  }

  @Override
  public void afterPropertiesSet() throws Exception {

    if (getDefaultMediaType() == null) {
      setDefaultMediaType(MediaType.APPLICATION_JSON_VALUE);
    }

    if (getPruneMode() == null) {
      setPruneMode(PruneMode.ALL);
    }

    if (getDataDirectory() == null) {
      setDataDirectory(new File(System.getProperty("user.home")));
    }

    if (!getDataDirectory().exists()) {
      getDataDirectory().mkdirs();
    }
  }
}
