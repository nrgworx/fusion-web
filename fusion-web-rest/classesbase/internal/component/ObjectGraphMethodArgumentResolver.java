package br.org.ccee.fusion.web.rest.internal.component;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.web.rest.api.component.ObjectGraphParser;
import br.org.ccee.fusion.web.rest.internal.util.ObjectGraphUtils;

public class ObjectGraphMethodArgumentResolver implements HandlerMethodArgumentResolver {

	private ObjectGraphParser objectGraphParser;

	public ObjectGraphMethodArgumentResolver(ObjectGraphParser objectGraphParser) {
		this.objectGraphParser = objectGraphParser;
	}

	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return methodParameter.getParameterType().equals(Optional.class)
				&& ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments()[0]
						.equals(ObjectGraph.class);
	}

	@Override
	public Object resolveArgument(
			MethodParameter parameter,
			ModelAndViewContainer modelAndViewContainer,
			NativeWebRequest nativeWebRequest,
			WebDataBinderFactory webDataBinderFactory) throws Exception {
		String expandExpression = nativeWebRequest.getParameter("$expand");
		if (expandExpression == null || expandExpression.isEmpty()) {
			return Optional.empty();
		} else {
			ObjectGraph objectGraph = this.objectGraphParser.parse(expandExpression);
			ObjectGraphUtils.saveObjectGraph(objectGraph);
			return Optional.of(objectGraph);
		}
	}

}
