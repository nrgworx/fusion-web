package br.org.ccee.fusion.web.rest.internal.component;

import java.time.Instant;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Path;

import br.org.ccee.fusion.commons.core.api.exception.UnexpectedException;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

@Component
public class ToDateTimeEntityFilterFunction extends AbstractPredicateFunction {

	private ObjectMapper objectMapper;

	public ToDateTimeEntityFilterFunction(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public String getName() {
		return "toDateTime";
	}

	@Override
	public Instant visit(Path<?> rootPath, ExpressionList parameterList) {
		String isoInstant = getParameter(rootPath, parameterList, 0).toString();
		try {
			return objectMapper.readValue(isoInstant, Instant.class);
		} catch (Exception e) {
			throw new UnexpectedException(String.format("Error parsing date %s to Instant", isoInstant));
		}
	}

}
