package br.org.ccee.fusion.web.rest.internal.component;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;

import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

@Component
public class MaxEntityFilterFunction extends AbstractPredicateFunction {

	@Override
	public String getName() {
		return "max";
	}

	@Override
	public Object visit(Path<?> rootPath, ExpressionList parameterList) {
		return Expressions.asString(getParameter(rootPath, parameterList, 0).toString()).max();
	}

}
