package br.org.ccee.fusion.web.rest.internal.component;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;

import br.org.ccee.fusion.web.rest.api.component.PredicateFunction;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.schema.Column;

public abstract class AbstractPredicateFunction implements PredicateFunction {

	protected Expression<String> getParameter(Path<?> rootPath, ExpressionList parameterList, int number) {

		if (parameterList == null || parameterList.getExpressions().isEmpty()) {
			throw new IllegalArgumentException("Parameter list is empty");
		}

		net.sf.jsqlparser.expression.Expression expression = parameterList.getExpressions().get(number);
		if (expression instanceof Column) {
			return Expressions.path(String.class, rootPath, ((Column) expression).getColumnName());
		}

		String strParam = expression.toString().trim();
		if (strParam.startsWith("'") && strParam.endsWith("'")) {
			strParam = strParam.substring(1, strParam.length() - 1);
		}

		return Expressions.constant(strParam);

	}

}
