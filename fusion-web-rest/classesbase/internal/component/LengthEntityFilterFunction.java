package br.org.ccee.fusion.web.rest.internal.component;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;

import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

@Component
public class LengthEntityFilterFunction extends AbstractPredicateFunction {

	@Override
	public String getName() {
		return "length";
	}

	@Override
	public Object visit(Path<?> rootPath, ExpressionList parameterList) {
		return Expressions.asString(getParameter(rootPath, parameterList, 0).toString()).length();
	}

}
