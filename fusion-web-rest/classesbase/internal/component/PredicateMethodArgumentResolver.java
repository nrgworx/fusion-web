package br.org.ccee.fusion.web.rest.internal.component;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

import org.springframework.core.GenericTypeResolver;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.controller.GenericController;
import br.org.ccee.fusion.web.rest.api.component.PredicateParser;

@Component
public class PredicateMethodArgumentResolver implements HandlerMethodArgumentResolver {

	private PredicateParser predicateParser;

	public PredicateMethodArgumentResolver(PredicateParser predicateParser) {
		this.predicateParser = predicateParser;
	}

	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return methodParameter.getParameterType().equals(Optional.class)
				&& ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments()[0]
						.equals(Predicate.class);
	}

	@Override
	public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
		String filterExpression = nativeWebRequest.getParameter("$filter");
		if (filterExpression == null || filterExpression.isEmpty()) {
			return Optional.empty();
		} else {
			Class<?> clazz = GenericTypeResolver.resolveTypeArguments(methodParameter.getDeclaringClass(), GenericController.class)[0];
			return Optional.of(this.predicateParser.parse(clazz, filterExpression));
		}
	}

}
