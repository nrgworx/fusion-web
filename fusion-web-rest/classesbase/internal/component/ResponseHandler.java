package br.org.ccee.fusion.web.rest.internal.component;

import java.security.InvalidParameterException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.core.MethodParameter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.org.ccee.fusion.commons.core.api.exception.DetailedException;
import br.org.ccee.fusion.commons.core.api.exception.IdentifiableException;
import br.org.ccee.fusion.commons.core.api.exception.NotAuthorizedException;
import br.org.ccee.fusion.commons.core.api.exception.NotFoundException;
import br.org.ccee.fusion.commons.core.api.exception.UnexpectedException;
import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.engine.controller.api.ObjectPruner;
import br.org.ccee.fusion.engine.controller.model.AsynchronousProcess;
import br.org.ccee.fusion.engine.controller.model.Response;
import br.org.ccee.fusion.engine.controller.util.ObjectGraphUtils;
import br.org.ccee.fusion.engine.controller.util.RequestMetadataUtils;
import br.org.ccee.fusion.engine.core.exception.GenericException;
import br.org.ccee.fusion.engine.core.exception.MultipleException;
import br.org.ccee.fusion.engine.core.util.EntityManagerUtils;
import br.org.ccee.fusion.engine.service.exception.TaskExecutionException;
import br.org.ccee.fusion.web.rest.api.model.FaultResponseMessage;
import br.org.ccee.fusion.web.rest.api.model.FaultResponseMessageItem;
import br.org.ccee.fusion.web.rest.api.model.PagedResponseMessage;
import br.org.ccee.fusion.web.rest.api.model.RequestMetadata;
import br.org.ccee.fusion.web.rest.api.model.ResponseMessage;
import br.org.ccee.fusion.web.rest.api.model.SimpleResponseMessage;

@ControllerAdvice
@ConditionalOnWebApplication
@Order(Ordered.LOWEST_PRECEDENCE)
public class ResponseHandler extends ResponseEntityExceptionHandler implements ResponseBodyAdvice<Object> {

	private static final Logger logger = LoggerFactory.getLogger(ResponseHandler.class);

	private ObjectPruner objectPruner;

	public ResponseHandler(ObjectPruner objectPruner) {
		this.objectPruner = objectPruner;
	}

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest serverHttpRequest,
			ServerHttpResponse serverHttpResponse) {

		Response<?> response = null;
		if (!isUserAuthenticated(body)) {
			return body;
		} else if (body instanceof FaultResponseMessage) {
			return body;
		} else if (body instanceof Resource) {
			return body;
		} else if (body instanceof Response) {
			response = (Response<?>) body;
		} else {
			response = new Response<>(body);
		}

		ServletServerHttpResponse servletServerHttpResponse = (ServletServerHttpResponse) serverHttpResponse;
		HttpStatus httpStatus = HttpStatus.valueOf(servletServerHttpResponse.getServletResponse().getStatus());

		Object payload = null;
		AsynchronousProcess asynchronousProcess = response.getAsynchronousProcess();
		if (asynchronousProcess != null) {
			payload = asynchronousProcess;
			serverHttpRequest.getHeaders().setAccept(Arrays.asList(serverHttpRequest.getHeaders().getContentType()));
			serverHttpResponse.setStatusCode(HttpStatus.ACCEPTED);
			serverHttpResponse.getHeaders().setContentType(serverHttpResponse.getHeaders().getContentType());
		} else {
			payload = response.getContent();
			cleanPayload(payload);
		}

		if (isResponseWrapped(selectedConverterType)) {
			return createResponseMessage(httpStatus, payload);
		} else {
			return createResponseMessage(payload);
		}
	}

	@ExceptionHandler({ NotAuthorizedException.class, HttpClientErrorException.Unauthorized.class })
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	protected ResponseEntity<FaultResponseMessage> handleUnauthorized(Exception exception, WebRequest webRequest) {
		logger.error(exception.getMessage(), exception);
		return this.createFaultResponseMessage(HttpStatus.UNAUTHORIZED, exception);
	}

	@ExceptionHandler({ InvalidParameterException.class, IllegalArgumentException.class, ConstraintViolationException.class, HttpClientErrorException.BadRequest.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<FaultResponseMessage> handleBadRequest(Exception exception, WebRequest webRequest) {
		logger.info(exception.getMessage(), exception);
		return this.createFaultResponseMessage(HttpStatus.BAD_REQUEST, exception);
	}

	@ExceptionHandler({ NotFoundException.class, EntityNotFoundException.class, JpaObjectRetrievalFailureException.class, HttpClientErrorException.NotFound.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	protected ResponseEntity<FaultResponseMessage> handleNotFound(Exception exception, WebRequest webRequest) {
		logger.info(exception.getMessage(), exception);
		return this.createFaultResponseMessage(HttpStatus.NOT_FOUND, exception);
	}

	@ExceptionHandler({ GenericException.class, TaskExecutionException.class, HttpClientErrorException.Forbidden.class })
	@ResponseStatus(HttpStatus.FORBIDDEN)
	protected ResponseEntity<FaultResponseMessage> handleForbidden(Exception exception, WebRequest webRequest) {
		logger.error(exception.getMessage(), exception);
		return this.createFaultResponseMessage(HttpStatus.FORBIDDEN, exception);
	}

	@ExceptionHandler({ Throwable.class, Exception.class, RuntimeException.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected ResponseEntity<FaultResponseMessage> handleInternalServerError(Throwable throwable, WebRequest webRequest) {
		logger.error(throwable.getMessage(), throwable);
		return this.createFaultResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR, throwable);
	}

	private boolean isUserAuthenticated(Object body) {
		if (body instanceof Map) {
			Map<?, ?> map = (Map<?, ?>) body;
			Integer status = (Integer) map.get("status");
			if (status != null && HttpStatus.UNAUTHORIZED.value() == status.intValue()) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	private void cleanPayload(Object payload) {
		Optional<ObjectGraph> objectGraph = ObjectGraphUtils.getObjectGraph();
		if (payload instanceof Page) {
			for (Object item : (Page<?>) payload) {
				try {
					detach(item);
				} catch (IllegalArgumentException illegalArgumentException) {
					logger.info(String.format(
							"Ignoring detach object %s from persistence context, this object is not an entity",
							item.getClass()));
				}
				this.objectPruner.prune(item, objectGraph);
			}
		} else if (payload instanceof Collection) {
			for (Object item : (Collection<?>) payload) {
				detach(item);
				this.objectPruner.prune(item, objectGraph);
			}
		} else {
			detach(payload);
			this.objectPruner.prune(payload, objectGraph);
		}
	}

	private void detach(Object object) {
		try {
			EntityManagerUtils.detach(object);
		} catch (IllegalArgumentException illegalArgumentException) {
			logger.info(
					String.format("Ignoring detach object %s from persistence context, this object is not an entity",
							object.getClass()));
		}
	}

	private boolean isResponseWrapped(Class<? extends HttpMessageConverter<?>> selectedConverterType) {
		if (selectedConverterType.getName().equals(CsvMessageConverter.class.getName())
				|| selectedConverterType.getName().equals(XlsxMessageConverter.class.getName())
				|| selectedConverterType.getName().equals(PdfMessageConverter.class.getName())) {
			return false;
		} else {
			return true;
		}
	}

	private Object createResponseMessage(Object payload) {
		if (payload instanceof Page) {
			Page<?> page = (Page<?>) payload;
			return page.getContent();
		} else {
			return payload;
		}
	}

	private ResponseMessage createResponseMessage(HttpStatus httpStatus, Object payload) {
		ResponseMessage responseMessage = null;
		if (payload instanceof Page<?>) {
			responseMessage = new PagedResponseMessage((Page<?>) payload);
		} else {
			responseMessage = new SimpleResponseMessage(payload);
		}
		setResponseMessageMetadata(httpStatus, responseMessage);
		return responseMessage;
	}

	private ResponseEntity<FaultResponseMessage> createFaultResponseMessage(HttpStatus httpStatus, Throwable throwable) {
		FaultResponseMessage faultResponseMessage = new FaultResponseMessage();
		setResponseMessageMetadata(httpStatus, faultResponseMessage);
		if (MultipleException.class.isAssignableFrom(throwable.getClass())) {
			MultipleException multipleException = (MultipleException) throwable;
			for (Throwable currentThrowable : multipleException.getThrowables()) {
				addFaultResponseMessageItem(currentThrowable, faultResponseMessage.getItems());
			}
		} else {
			addFaultResponseMessageItem(throwable, faultResponseMessage.getItems());
		}
		return new ResponseEntity<>(faultResponseMessage, httpStatus);
	}

	private void setResponseMessageMetadata(HttpStatus httpStatus, ResponseMessage responseMessage) {
		RequestMetadata requestMetadata = RequestMetadataUtils.getRequestMetadata()
				.orElseThrow(() -> new UnexpectedException("Unable to get request metadata"));
		responseMessage.setId(requestMetadata.getId().toString());
		responseMessage.setResource(requestMetadata.getResource());
		responseMessage.setInstant(requestMetadata.getInstant());
		responseMessage.setDuration(ChronoUnit.MILLIS.between(requestMetadata.getInstant(), Instant.now()));
		responseMessage.setStatus(httpStatus.value());
	}

	private void addFaultResponseMessageItem(Throwable throwable,
			List<FaultResponseMessageItem> faultResponseMessageItems) {
		faultResponseMessageItems.add(createFaultResponseMessageItem(throwable));
		Throwable cause = throwable.getCause();
		if (cause != null) {
			addFaultResponseMessageItem(cause, faultResponseMessageItems);
		}
	}

	private FaultResponseMessageItem createFaultResponseMessageItem(Throwable throwable) {
		FaultResponseMessageItem faultResponseMessageItem = new FaultResponseMessageItem();
		if (IdentifiableException.class.isAssignableFrom(throwable.getClass())) {
			faultResponseMessageItem.setCode(((IdentifiableException) throwable).getCode());
		}
		faultResponseMessageItem
				.setTitle(throwable.getMessage() == null ? throwable.getClass().getName() : throwable.getMessage());
		if (DetailedException.class.isAssignableFrom(throwable.getClass())) {
			faultResponseMessageItem.setDetail(((DetailedException) throwable).getDetail());
		}
		faultResponseMessageItem.setMeta(throwable);
		return faultResponseMessageItem;
	}

}
