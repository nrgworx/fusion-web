package br.org.ccee.fusion.web.rest.internal.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.commons.core.api.model.ObjectGraphNode;
import br.org.ccee.fusion.web.rest.api.component.ObjectGraphParser;
import br.org.ccee.fusion.web.rest.api.exception.InvalidExpandExpressionException;

@Component
@ConditionalOnSingleCandidate(ObjectGraphParser.class)
public class DefaultObjectGraphParser implements ObjectGraphParser {

	@Override
	public ObjectGraph parse(String expandExpression) throws InvalidExpandExpressionException {
		Assert.notNull(expandExpression, "Expand expression must not be null");
		return new ObjectGraph(parseExpandExpression(expandExpression.replaceAll("\\s+", "")));
	}

	private List<ObjectGraphNode> parseExpandExpression(String expandExpression) {
		List<ObjectGraphNode> objectGraphNodes = new ArrayList<>();
		for (String expandPath : expandExpression.split(",")) {
			List<String> pathItems = new LinkedList<String>(Arrays.asList(expandPath.split("\\.")));
			ObjectGraphNode objectGraphNode = parseExpandPath(pathItems);
			merge(objectGraphNode, objectGraphNodes);
		}
		return objectGraphNodes;
	}

	private ObjectGraphNode parseExpandPath(List<String> expandPathItems) {
		ObjectGraphNode objectGraphNode = new ObjectGraphNode(expandPathItems.get(0));
		if (expandPathItems.size() > 1) {
			expandPathItems.remove(0);
			objectGraphNode.addChild(parseExpandPath(expandPathItems));
		}
		return objectGraphNode;
	};

	private void merge(ObjectGraphNode objectGraphNode, List<ObjectGraphNode> objectGraphNodes) {
		ObjectGraphNode childObjectGraphNode = get(objectGraphNode.getName(), objectGraphNodes);
		// if (objectGraphNode.getChildren() != null &&
		// !objectGraphNode.getChildren().isEmpty() && childObjectGraphNode != null) {
		if (childObjectGraphNode != null) {
			merge(objectGraphNode.getChildren().get(0), childObjectGraphNode.getChildren());
		} else {
			objectGraphNodes.add(objectGraphNode);
		}
	}

	private ObjectGraphNode get(String name, List<ObjectGraphNode> objectGraphNodes) {
		for (ObjectGraphNode objectGraphNode : objectGraphNodes) {
			if (objectGraphNode.getName().equals(name)) {
				return objectGraphNode;
			}
		}
		return null;
	}

}
