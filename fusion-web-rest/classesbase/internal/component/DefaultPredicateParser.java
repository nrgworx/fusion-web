package br.org.ccee.fusion.web.rest.internal.component;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.data.querydsl.QuerydslUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableMap;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimplePath;

import br.org.ccee.fusion.commons.core.api.util.ApplicationContextUtils;
import br.org.ccee.fusion.web.rest.api.component.PredicateExpressionVisitor;
import br.org.ccee.fusion.web.rest.api.component.PredicateParser;
import br.org.ccee.fusion.web.rest.api.exception.InvalidFilterExpressionException;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;

@Component
@ConditionalOnSingleCandidate(PredicateParser.class)
public class DefaultPredicateParser implements PredicateParser {

	private static final Map<String, String> definitions = ImmutableMap.<String, String>builder().put(" eq ", " = ")
			.put(" lt ", " < ").put(" goe ", " >= ").put(" loe ", " <= ").put(" gt ", " > ").put(" EQ ", " = ")
			.put(" LT ", " < ").put(" GOE ", " >= ").put(" LOE ", " <= ").put(" GT ", " > ").build();

	@Override
	public Predicate parse(Class<?> type, String filterExpression) throws InvalidFilterExpressionException {

		Assert.notNull(type, "Parameter type must not be null");
		Assert.notNull(type, "Parameter filterExpression must not be null");

		Expression expression;
		try {
			expression = CCJSqlParserUtil.parseCondExpression(sanitize(filterExpression));
		} catch (JSQLParserException jsqlParserException) {
			throw new InvalidFilterExpressionException(jsqlParserException.getMessage(), jsqlParserException);
		}

		PredicateExpressionVisitor entityFilterParser = ApplicationContextUtils
				.getBean(PredicateExpressionVisitor.class);
		SimplePath<?> rootPath = QuerydslUtils.createRootPath(type);
		entityFilterParser.init(rootPath);
		expression.accept(entityFilterParser);
		return entityFilterParser.getPredicate();
	}

	private String sanitize(String filterExpression) {
		final String[] keys = this.definitions.keySet().toArray(new String[this.definitions.size()]);
		final String[] values = this.definitions.values().toArray(new String[this.definitions.size()]);
		return StringUtils.replaceEach(filterExpression, keys, values);
	}
}
