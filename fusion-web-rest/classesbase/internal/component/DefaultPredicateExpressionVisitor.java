package br.org.ccee.fusion.web.rest.internal.component;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.SimplePath;

import br.org.ccee.fusion.commons.core.api.exception.NotImplementedException;
import br.org.ccee.fusion.engine.controller.util.PredicateFunctionUtils;
import br.org.ccee.fusion.web.rest.api.component.PredicateExpressionVisitor;
import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnalyticExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.CastExpression;
import net.sf.jsqlparser.expression.CollateExpression;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.ExtractExpression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.HexValue;
import net.sf.jsqlparser.expression.IntervalExpression;
import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.JsonExpression;
import net.sf.jsqlparser.expression.KeepExpression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.MySQLGroupConcat;
import net.sf.jsqlparser.expression.NextValExpression;
import net.sf.jsqlparser.expression.NotExpression;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.NumericBind;
import net.sf.jsqlparser.expression.OracleHierarchicalExpression;
import net.sf.jsqlparser.expression.OracleHint;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.RowConstructor;
import net.sf.jsqlparser.expression.SignedExpression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeKeyExpression;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.UserVariable;
import net.sf.jsqlparser.expression.ValueListExpression;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseLeftShift;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseRightShift;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Modulo;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.JsonOperator;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.expression.operators.relational.NamedExpressionList;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.expression.operators.relational.RegExpMatchOperator;
import net.sf.jsqlparser.expression.operators.relational.RegExpMySQLOperator;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SubSelect;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@ConditionalOnSingleCandidate(PredicateExpressionVisitor.class)
public class DefaultPredicateExpressionVisitor implements PredicateExpressionVisitor {

	private SimplePath<?> rootPath;
	private Stack<Object> stack;

	@Override
	public void init(SimplePath<?> rootPath) {
		this.rootPath = rootPath;
		this.stack = new Stack<Object>();
	}

	@Override
	public void visit(BitwiseRightShift aThis) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(BitwiseLeftShift aThis) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(NullValue nullValue) {
		this.stack.push(Expressions.nullExpression());
	}

	@Override
	public void visit(Function function) {
		this.stack.push(PredicateFunctionUtils.getPredicateFunction(function.getName()).visit(rootPath,
				function.getParameters()));
	}

	@Override
	public void visit(SignedExpression signedExpression) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(JdbcParameter jdbcParameter) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(JdbcNamedParameter jdbcNamedParameter) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(DoubleValue doubleValue) {
		this.stack.push(doubleValue.getValue());
	}

	@Override
	public void visit(LongValue longValue) {
		this.stack.push(longValue.getValue());
	}

	@Override
	public void visit(HexValue hexValue) {
		this.stack.push(hexValue.getValue());
	}

	@Override
	public void visit(DateValue dateValue) {
		this.stack.push(dateValue.getValue());
	}

	@Override
	public void visit(TimeValue timeValue) {
		this.stack.push(timeValue.getValue());
	}

	@Override
	public void visit(TimestampValue timestampValue) {
		this.stack.push(timestampValue.getValue());
	}

	@Override
	public void visit(Parenthesis parenthesis) {
		parenthesis.getExpression().accept(this);
	}

	@Override
	public void visit(StringValue stringValue) {
		if (stringValue.equals("true")) {
			this.stack.push(true);
		} else if (stringValue.equals("false")) {
			this.stack.push(false);
		} else {
			this.stack.push(stringValue.getValue());
		}
	}

	@Override
	public void visit(Addition addition) {
		// TODO Auto-generated method stub
		// this.stack.push(this.stack.pop() + this.stack.pop());
		throw new NotImplementedException();
	}

	@Override
	public void visit(Division division) {
		// TODO Auto-generated method stub
		// this.stack.push(this.stack.pop() / this.stack.pop());
		throw new NotImplementedException();
	}

	@Override
	public void visit(Multiplication multiplication) {
		// TODO Auto-generated method stub
		// this.stack.push(this.stack.pop() * this.stack.pop());
		throw new NotImplementedException();
	}

	@Override
	public void visit(Subtraction subtraction) {
		// TODO Auto-generated method stub
		// this.stack.push(this.stack.pop() - this.stack.pop());
		throw new NotImplementedException();
	}

	@Override
	public void visit(AndExpression andExpression) {
		binaryExpression(andExpression, Ops.AND);
	}

	@Override
	public void visit(OrExpression orExpression) {
		binaryExpression(orExpression, Ops.OR);
	}

	@Override
	public void visit(Between between) {
		// TODO Auto-generated method stub
		// binaryExpression(between, Ops.BETWEEN);
		throw new NotImplementedException();
	}

	@Override
	public void visit(EqualsTo equalsTo) {
		binaryExpression(equalsTo, Ops.EQ);
	}

	@Override
	public void visit(GreaterThan greaterThan) {
		binaryExpression(greaterThan, Ops.GT);
	}

	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		binaryExpression(greaterThanEquals, Ops.GOE);
	}

	@Override
	public void visit(InExpression inExpression) {
		inExpression(inExpression, Ops.IN);
	}

	@Override
	public void visit(IsNullExpression isNullExpression) {
		// comparisonExpression(isNullExpression, Ops.IS_NULL);
		throw new NotImplementedException();
	}

	@Override
	public void visit(LikeExpression likeExpression) {
		binaryExpression(likeExpression, Ops.LIKE);
	}

	@Override
	public void visit(MinorThan minorThan) {
		binaryExpression(minorThan, Ops.LT);
	}

	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		binaryExpression(minorThanEquals, Ops.LOE);
	}

	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		binaryExpression(notEqualsTo, Ops.NE);
	}

	@Override
	public void visit(Column tableColumn) {
		this.stack.push(tableColumn);
	}

	@Override
	public void visit(SubSelect subSelect) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(CaseExpression caseExpression) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(WhenClause whenClause) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(ExistsExpression existsExpression) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(AllComparisonExpression allComparisonExpression) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(AnyComparisonExpression anyComparisonExpression) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(Concat concat) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(Matches matches) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(BitwiseAnd bitwiseAnd) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(BitwiseOr bitwiseOr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(BitwiseXor bitwiseXor) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(CastExpression cast) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(Modulo modulo) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(AnalyticExpression aexpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(ExtractExpression eexpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(IntervalExpression iexpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(OracleHierarchicalExpression oexpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(RegExpMatchOperator rexpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(JsonExpression jsonExpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(JsonOperator jsonExpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(RegExpMySQLOperator regExpMySQLOperator) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(UserVariable var) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(NumericBind bind) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(KeepExpression aexpr) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(MySQLGroupConcat groupConcat) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(ValueListExpression valueList) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(RowConstructor rowConstructor) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(OracleHint hint) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(TimeKeyExpression timeKeyExpression) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(DateTimeLiteralExpression literal) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(NotExpression aThis) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(ExpressionList expressionList) {
		this.stack.push(Expressions.anyOf((BooleanExpression[]) expressionList.getExpressions().toArray()));
	}

	@Override
	public void visit(MultiExpressionList multiExprList) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(NamedExpressionList namedExpressionList) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(NextValExpression aThis) {
		throw new NotImplementedException();
	}

	@Override
	public void visit(CollateExpression aThis) {
		throw new NotImplementedException();
	}

	@Override
	public Predicate getPredicate() {
		BooleanExpression booleanExpression = (BooleanExpression) this.stack.pop();
		this.stack = new Stack<Object>();
		return booleanExpression;
	}

	private void binaryExpression(BinaryExpression binaryExpression, Ops ops) {
		binaryExpression.getLeftExpression().accept(this);
		binaryExpression.getRightExpression().accept(this);
		Object right = this.stack.pop();
		Object left = this.stack.pop();
		this.stack.push(Expressions.predicate(ops, getExpression(left), getExpression(right)));
	}

	private void inExpression(InExpression inExpression, Ops ops) {
		inExpression.getLeftExpression().accept(this);
		inExpression.getRightItemsList().accept(this);
		Object right = this.stack.pop();
		Object left = this.stack.pop();
		this.stack.push(Expressions.predicate(Ops.IN, getExpression(left), getExpression(right)));
	}

	private Expression<?> getExpression(Object param) {
		try {
			if (param instanceof Expression<?>) {
				return getExpression((Expression<?>) param);
			} else if (param instanceof Column) {
				return getExpression((Column) param);
			} else {
				return Expressions.constant(param);
			}
		} catch (Throwable throwable) {
			throw new RuntimeException(throwable);
		}
	}

	private Expression<?> getExpression(Expression<?> expression) {
		return expression;
	}

	private Expression<?> getExpression(Column column) throws NoSuchFieldException, SecurityException {
		if (column.getFullyQualifiedName().contains(".")) {
			List<String> attributeNames = new LinkedList<String>(Arrays.asList(column.getFullyQualifiedName().split("\\.")));
			return getExpression(attributeNames);
		} else {
			return Expressions.path(String.class, this.rootPath, column.getFullyQualifiedName());
		}
	}

	private Expression<?> getExpression(List<String> attributeNames) throws NoSuchFieldException, SecurityException {
		Class<?> type = this.rootPath.getType();
		String attributeName = this.rootPath.getMetadata().getName();
		PathBuilder<?> pathBuilder = new PathBuilder(type, attributeName);
		do {
			attributeName = attributeNames.remove(0);
			Field field = type.getDeclaredField(attributeName);
			type = getFieldClass(field);
			Set<Class<?>> implementedInterfaces = ReflectionUtils.getImplementedInterfaces(field.getType());
			if (field.getType().equals(Set.class) || implementedInterfaces.contains(Set.class)) {
				pathBuilder = pathBuilder.getSet(attributeName, type).any();
			} else if (field.getType().equals(List.class) || implementedInterfaces.contains(List.class)) {
				pathBuilder = pathBuilder.getList(attributeName, type).any();
			} else if (field.getType().equals(Collection.class) || implementedInterfaces.contains(Collection.class)) {
				pathBuilder = pathBuilder.getCollection(attributeName, type).any();
			} else {
				pathBuilder = pathBuilder.get(attributeName);
			}
		} while (!attributeNames.isEmpty());
		return pathBuilder;
	}

	private Class<?> getFieldClass(Field field) {
		Class<?> clazz = null;
		if (ReflectionUtils.getImplementedInterfaces(field.getType()).contains(Collection.class)
				&& field.getGenericType() instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
			clazz = (Class<?>) parameterizedType.getActualTypeArguments()[0];
		} else {
			clazz = field.getType();
		}
		return clazz;
	}

}
