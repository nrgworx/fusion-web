package br.org.ccee.fusion.web.rest.internal.component;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimplePath;

import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

@Component
public class ToLowerEntityFilterFunction extends AbstractPredicateFunction {

	@Override
	public String getName() {
		return "toLower";
	}

	@Override
	public Object visit(Path<?> rootPath, ExpressionList parameterList) {
		Expression<String> expression = getParameter(rootPath, parameterList, 0);
		if (expression instanceof SimplePath<?>) {
			return Expressions.stringOperation(Ops.LOWER, expression);
		}
		return Expressions.asString(expression.toString()).lower();
	}

}
