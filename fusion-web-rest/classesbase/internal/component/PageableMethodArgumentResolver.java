package br.org.ccee.fusion.web.rest.internal.component;

import java.util.Optional;
import java.util.OptionalInt;

import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import br.org.ccee.fusion.web.rest.api.component.PageableParser;

public class PageableMethodArgumentResolver implements HandlerMethodArgumentResolver {

	private PageableParser pageableParser;

	public PageableMethodArgumentResolver(PageableParser pageableParser) {
		this.pageableParser = pageableParser;
	}

	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return methodParameter.getParameterType().equals(Pageable.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer modelAndViewContainer,
			NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
		return this.pageableParser.parse(getPageNumber(nativeWebRequest), getPageSize(nativeWebRequest),
				getSort(nativeWebRequest));
	}

	private OptionalInt getPageNumber(NativeWebRequest nativeWebRequest) {
		String pageNumber = nativeWebRequest.getParameter("$pageNumber");
		if (pageNumber != null) {
			return OptionalInt.of(Integer.parseInt(pageNumber));
		} else {
			return OptionalInt.empty();
		}
	}

	private OptionalInt getPageSize(NativeWebRequest nativeWebRequest) {
		String pageSize = nativeWebRequest.getParameter("$pageSize");
		if (pageSize != null) {
			return OptionalInt.of(Integer.parseInt(pageSize));
		} else {
			return OptionalInt.empty();
		}
	}

	private Optional<String> getSort(NativeWebRequest nativeWebRequest) {
		String sort = nativeWebRequest.getParameter("$sort");
		if (sort != null) {
			return Optional.of(sort);
		} else {
			return Optional.empty();
		}
	}

}
