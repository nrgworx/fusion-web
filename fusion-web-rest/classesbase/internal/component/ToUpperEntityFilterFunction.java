package br.org.ccee.fusion.web.rest.internal.component;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimplePath;

import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

@Component
public class ToUpperEntityFilterFunction extends AbstractPredicateFunction {

	@Override
	public String getName() {
		return "toUpper";
	}

	@Override
	public Object visit(Path<?> rootPath, ExpressionList parameterList) {
		Expression<String> expression = getParameter(rootPath, parameterList, 0);
		if (expression instanceof SimplePath<?>) {
			return Expressions.stringOperation(Ops.UPPER, expression);
		}
		return Expressions.asString(expression.toString()).upper();
	}

}
