package br.org.ccee.fusion.web.rest.api.component;

import com.querydsl.core.types.Path;

import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

/**
 * This class is responsible to define the visitor methods used to parse an function in the entity
 * filter expression.
 * 
 * @author thiago.assis
 */
public interface PredicateFunction {

  /**
   * @return filter name
   */
  String getName();

  /**
   * @param rootPath a root path
   * @param parameterList list the filter parameter
   * @return an Object according to the implementation
   */
  Object visit(Path<?> rootPath, ExpressionList parameterList);

}
