package br.org.ccee.fusion.web.rest.api.component;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimplePath;

import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;

/**
 * This class is responsible to define the visitor methods used to parse the
 * entity filter expression.
 * 
 * @since 1.0.0
 * @author thiago.assis
 */
public interface PredicateExpressionVisitor extends ExpressionVisitor, ItemsListVisitor {

	void init(final SimplePath<?> rootPath);

	/**
	 * @return a predicate to filter implementation.
	 */
	Predicate getPredicate();

}
