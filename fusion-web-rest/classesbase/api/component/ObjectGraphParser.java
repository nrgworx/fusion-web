package br.org.ccee.fusion.web.rest.api.component;

import java.util.Optional;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.web.rest.api.exception.InvalidExpandExpressionException;

/**
 * This class is responsible to define the methods used to handle the entity
 * expansions feature.
 *
 * @since 1.0.0
 * @author thiago.assis
 */
public interface ObjectGraphParser {

	/**
	 * This method is responsible for transform an expand expression in an String
	 * format to an EntityGraph used in a query.
	 * 
	 * @see EntityGraph
	 * 
	 * @param entityManager            an EntityManager
	 * @param jpaEntityMetadata        a JpaEntityMetada
	 * @param optionalExpandExpression send to expression to expand children
	 * @return an Optional EntityGraph
	 * @see Optional
	 * @throws InvalidExpandExpressionException when no match expand expression
	 * 
	 * @since 1.0.0
	 * @author thiago.assis
	 */
	ObjectGraph parse(final String expandExpression) throws InvalidExpandExpressionException;

}
