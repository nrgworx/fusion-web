package br.org.ccee.fusion.web.rest.api.model;

import java.net.URI;
import java.time.Instant;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Basic POJO return for all REST APIs.
 * 
 * @see {@link SimpleResponseMessage}, {@link PagedResponseMessage}
 *
 */
public abstract class Message {

	@JsonProperty("id")
	@JacksonXmlProperty(localName = "id")
	private String id;

	@JsonProperty("resource")
	@JacksonXmlProperty(localName = "resource")
	private URI resource;

	@JsonProperty("instant")
	@JacksonXmlProperty(localName = "instant")
	private Instant instant;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public URI getResource() {
		return resource;
	}

	public void setResource(URI resource) {
		this.resource = resource;
	}

	public Instant getInstant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}
}
