package br.org.ccee.fusion.web.rest.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Application specific error.
 */
@JsonInclude(Include.NON_NULL)
public class FaultResponseMessageItem {

    /**
     * An application-specific error code.
     */
    @JsonProperty("code")
    private String code;

    /**
     * A short, human-readable summary of the error.
     */
    @JsonProperty("title")
    private String title;

    /**
     * A human-readable explanation specific of the error.
     */
    @JsonProperty("detail")
    private String detail;

    /**
     * Meta object containing non-standard meta-information about the error.
     */
    @JsonProperty("meta")
    private Object meta;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }

  /*@Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }*/
}
