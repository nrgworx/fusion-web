package br.org.ccee.fusion.web.rest.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Return POJO for all REST APIs when it result is an object (primitive, domian
 * or list).
 *
 */
public final class SimpleResponseMessage extends ResponseMessage {

	@JsonProperty("content")
	@JacksonXmlProperty(localName = "content")
	private Object content;

	public SimpleResponseMessage() {

	}

	public SimpleResponseMessage(Object content) {
		this.content = content;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}
}
