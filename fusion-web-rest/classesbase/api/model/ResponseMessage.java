package br.org.ccee.fusion.web.rest.api.model;

import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public abstract class ResponseMessage extends Message {

	@JsonProperty("duration")
	@JacksonXmlProperty(localName = "duration")
	private Long duration;

	@JsonProperty("status")
	@JacksonXmlProperty(localName = "status")
	private Integer status;

	public ResponseMessage() {
		setId(UUID.randomUUID().toString());
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
