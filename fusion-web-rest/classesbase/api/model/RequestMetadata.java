package br.org.ccee.fusion.web.rest.api.model;

import java.net.URI;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class RequestMetadata {

  private String id;

  private URI resource;

  private Instant instant;

  private String user;

  public RequestMetadata() {

  }

  public RequestMetadata(HttpServletRequest httpServletRequest) {
    this.id = UUID.randomUUID().toString();
    this.resource = URI.create(httpServletRequest.getRequestURL()
        .append(Optional.ofNullable(httpServletRequest.getQueryString()).orElse("")).toString());
    this.instant = Instant.now();
    SecurityContext securityContext = SecurityContextHolder.getContext();
    if (securityContext != null) {
      Authentication authentication = securityContext.getAuthentication();
      if (authentication != null) {
        this.user = authentication.getName();
      }
    }
  }

  public String getId() {
    return id;
  }

  public URI getResource() {
    return resource;
  }

  public Instant getInstant() {
    return instant;
  }

  public String getUser() {
    return user;
  }
}
