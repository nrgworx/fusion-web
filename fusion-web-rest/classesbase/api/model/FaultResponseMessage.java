package br.org.ccee.fusion.web.rest.api.model;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Rest Error object.
 */
@JsonInclude(Include.NON_NULL)
public class FaultResponseMessage extends ResponseMessage {

	/**
	 * Fault items.
	 */
	@JsonProperty("items")
	private List<FaultResponseMessageItem> items = new ArrayList<>();

	/**
	 * Meta object containing non-standard meta-information about the error.
	 */
	@JsonProperty("meta")
	private Object meta;

	public List<FaultResponseMessageItem> getItems() {
		return items;
	}

	public void setItems(List<FaultResponseMessageItem> items) {
		this.items = items;
	}

	public Object getMeta() {
		return meta;
	}

	public void setMeta(Object meta) {
		this.meta = meta;
	}

}
