package br.org.ccee.fusion.web.rest.api.model;

import org.springframework.data.domain.Page;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Return POJO for all REST APIs when it result is an paged list (Page).
 *
 */
public final class PagedResponseMessage extends ResponseMessage {

	@JsonUnwrapped
	private Page<?> page;

	public PagedResponseMessage() {
	}

	public PagedResponseMessage(Page<?> page) {
		this.page = page;
	}

	public Page<?> getPage() {
		return page;
	}

	public void setPage(Page<?> page) {
		this.page = page;
	}

}
